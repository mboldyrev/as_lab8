/**
 * Data models
 */
Apperyio.Entity = new Apperyio.EntityFactory({
    "String": {
        "type": "string"
    },
    "Number": {
        "type": "number"
    },
    "Boolean": {
        "type": "boolean"
    }
});
Apperyio.getModel = Apperyio.Entity.get.bind(Apperyio.Entity);

/**
 * Data storage
 */
Apperyio.storage = {

    "lng": new $a.LocalStorage("lng", "String"),

    "lat": new $a.LocalStorage("lat", "String"),

    "lng_0": new $a.LocalStorage("lng_0", "String"),

    "lat_0": new $a.LocalStorage("lat_0", "String")
};