/*
 * Security contexts
 */

/*
 * Service settings
 */

/*
 * Services
 */

GenericService = new Apperyio.auth({

    "serviceName": "GenericService"

    ,
    'defaultRequest': {
        "data": null
    }
});

var WeatherAPI_service = new Apperyio.RestService({
    'url': 'https://api.appery.io/rest/1/code/bb88518e-5335-4961-8ed8-7472b3ef4a5e/exec',
    'dataType': 'json',
    'type': 'get',

    'defaultRequest': {
        "headers": {
            "X-Appery-Api-Express-Api-Key": ""
        },
        "parameters": {},
        "body": null
    }
});

var GeolocationService = new Apperyio.GeolocationService({
    'defaultRequest': {
        "data": {
            "options": {
                "maximumAge": 3000,
                "timeout": 5000,
                "enableHighAccuracy": true,
                "watchPosition": false
            }
        }
    }
});