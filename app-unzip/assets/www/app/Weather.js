/*
 * JS for Weather generated by Appery.io
 */

Apperyio.getProjectGUID = function() {
    return '47fefdf8-0aeb-46c2-a95e-d8f6c3864be8';
};

function navigateTo(outcome, useAjax) {
    Apperyio.navigateTo(outcome, useAjax);
}

function adjustContentHeight() {
    Apperyio.adjustContentHeightWithPadding();
}

function adjustContentHeightWithPadding(_page) {
    Apperyio.adjustContentHeightWithPadding(_page);
}

function setDetailContent(pageUrl) {
    Apperyio.setDetailContent(pageUrl);
}

Apperyio.AppPages = [{
    "name": "startScreen",
    "location": "startScreen.html"
}, {
    "name": "Weather_Weather_API_and_Geolocation_App",
    "location": "Weather_Weather_API_and_Geolocation_App.html"
}, {
    "name": "Weather",
    "location": "Weather.html"
}];

function Weather_js() {

    /* Object & array with components "name-to-id" mapping */
    var n2id_buf = {
        'mobilegrid_3': 'Weather_mobilegrid_3',
        'mobilegridcell_4': 'Weather_mobilegridcell_4',
        'input': 'Weather_input',
        'mobilegridcell_5': 'Weather_mobilegridcell_5',
        'mobilebutton_10': 'Weather_mobilebutton_10',
        'mobilebutton_11': 'Weather_mobilebutton_11',
        'result_list': 'Weather_result_list',
        'city': 'Weather_city',
        'mobilelistitembutton_17': 'Weather_mobilelistitembutton_17',
        'temperature': 'Weather_temperature',
        'mobilelistitembutton_19': 'Weather_mobilelistitembutton_19',
        'weather': 'Weather_weather',
        'mobilelistitembutton_21': 'Weather_mobilelistitembutton_21',
        'icon': 'Weather_icon',
        'mobilelistitembutton_23': 'Weather_mobilelistitembutton_23',
        'mobileimage_24': 'Weather_mobileimage_24'
    };

    if ("n2id" in window && window.n2id !== undefined) {
        $.extend(n2id, n2id_buf);
    } else {
        window.n2id = n2id_buf;
    }

    /*
     * Nonvisual components
     */

    Apperyio.mappings = Apperyio.mappings || {};

    Apperyio.mappings["Weather_geolocation_onsuccess_mapping_0"] = {
        "homeScreen": "Weather",
        "directions": [

        {
            "from_name": "geolocation",
            "from_type": "SERVICE_RESPONSE",

            "to_name": "lat",
            "to_type": "LOCAL_STORAGE",

            "mappings": [

            {

                "source": "$['data']['coords']['latitude']",
                "target": "$"

            }

            ]
        },

        {
            "from_name": "geolocation",
            "from_type": "SERVICE_RESPONSE",

            "to_name": "lng",
            "to_type": "LOCAL_STORAGE",

            "mappings": [

            {

                "source": "$['data']['coords']['longitude']",
                "target": "$"

            }

            ]
        }

        ]
    };

    Apperyio.mappings["Weather_geolocation_onbeforesend_mapping_0"] = {
        "homeScreen": "Weather",
        "directions": [

        {

            "to_name": "geolocation",
            "to_type": "SERVICE_REQUEST",

            "to_default": {
                "data": {
                    "options": {
                        "maximumAge": 3000,
                        "timeout": 5000,
                        "enableHighAccuracy": true,
                        "watchPosition": false
                    }
                }
            },

            "mappings": []
        }

        ]
    };

    Apperyio.mappings["Weather_weather_onsuccess_mapping_0"] = {
        "homeScreen": "Weather",
        "directions": [

        {
            "from_name": "weather",
            "from_type": "SERVICE_RESPONSE",

            "to_name": "Weather",
            "to_type": "UI",

            "mappings": [

            {

                "source": "$['body']['current_observation']['display_location']['full']",
                "target": "$['city:text']"

            },

            {

                "source": "$['body']['current_observation']['temperature_string']",
                "target": "$['temperature:text']"

            },

            {

                "source": "$['body']['current_observation']['weather']",
                "target": "$['weather:text']"

            },

            {

                "source": "$['body']['current_observation']['icon_url']",
                "target": "$['mobileimage_24:image']"

            }

            ]
        }

        ]
    };

    Apperyio.mappings["Weather_weather_onbeforesend_mapping_0"] = {
        "homeScreen": "Weather",
        "directions": [

        {
            "from_name": "Weather",
            "from_type": "UI",

            "to_name": "weather",
            "to_type": "SERVICE_REQUEST",

            "to_default": {
                "headers": {},
                "parameters": {},
                "body": null
            },

            "mappings": [

            {

                "source": "$['input:text']",
                "target": "$['parameters']['location']"

            }

            ]
        }

        ]
    };

    Apperyio.datasources = Apperyio.datasources || {};

    window.geolocation = Apperyio.datasources.geolocation = new Apperyio.DataSource(GeolocationService, {
        "onBeforeSend": function(jqXHR) {
            Apperyio.processMappingAction(Apperyio.mappings["Weather_geolocation_onbeforesend_mapping_0"]);
        },
        "onComplete": function(jqXHR, textStatus) {

        },
        "onSuccess": function(data) {
            Apperyio.processMappingAction(Apperyio.mappings["Weather_geolocation_onsuccess_mapping_0"]);
            Apperyio("input").val(localStorage.getItem("lat") + "," + localStorage.getItem("lng"));
        },
        "onError": function(jqXHR, textStatus, errorThrown) {}
    });

    window.weather = Apperyio.datasources.weather = new Apperyio.DataSource(WeatherAPI_service, {
        "onBeforeSend": function(jqXHR) {
            Apperyio.processMappingAction(Apperyio.mappings["Weather_weather_onbeforesend_mapping_0"]);
        },
        "onComplete": function(jqXHR, textStatus) {

        },
        "onSuccess": function(data) {
            Apperyio.processMappingAction(Apperyio.mappings["Weather_weather_onsuccess_mapping_0"]);
            Apperyio("result_list").show();
        },
        "onError": function(jqXHR, textStatus, errorThrown) {}
    });

    Apperyio.CurrentScreen = 'Weather';
    _.chain(Apperyio.mappings).filter(function(m) {
        return m.homeScreen === Apperyio.CurrentScreen;
    }).each(Apperyio.UIHandler.hideTemplateComponents);

    /*
     * Events and handlers
     */

    // On Load
    var Weather_onLoad = function() {
            Weather_elementsExtraJS();

            Weather_deviceEvents();
            Weather_windowEvents();
            Weather_elementsEvents();
        };

    // screen window events


    function Weather_windowEvents() {

        $('#Weather').bind('pageshow orientationchange', function() {
            var _page = this;
            adjustContentHeightWithPadding(_page);
        });

    };

    // device events


    function Weather_deviceEvents() {
        document.addEventListener("deviceready", function() {

        });
    };

    // screen elements extra js


    function Weather_elementsExtraJS() {
        // screen (Weather) extra code

        /* result_list */

        listView = $("#Weather_result_list");
        theme = listView.attr("data-theme");
        if (typeof theme !== 'undefined') {
            var themeClass = "ui-btn-up-" + theme;
            listItem = $("#Weather_result_list .ui-li-static");
            $.each(listItem, function(index, value) {
                $(this).addClass(themeClass);
            });
        }

        /* city */

        /* temperature */

        /* weather */

        /* icon */

    };

    // screen elements handler


    function Weather_elementsEvents() {
        $(document).on("click", "a :input,a a,a fieldset label", function(event) {
            event.stopPropagation();
        });

        $(document).off("click", '#Weather_mobilecontainer1 [name="mobilebutton_10"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    try {
                        geolocation.execute({});
                    } catch (e) {
                        console.error(e);
                        hideSpinner();
                    };

                }
            },
        }, '#Weather_mobilecontainer1 [name="mobilebutton_10"]');
        $(document).off("click", '#Weather_mobilecontainer1 [name="mobilebutton_11"]').on({
            click: function(event) {
                if (!$(this).attr('disabled')) {
                    try {
                        weather.execute({});
                    } catch (e) {
                        console.error(e);
                        hideSpinner();
                    };

                }
            },
        }, '#Weather_mobilecontainer1 [name="mobilebutton_11"]');

    };

    $(document).off("pagebeforeshow", "#Weather").on("pagebeforeshow", "#Weather", function(event, ui) {
        Apperyio.CurrentScreen = "Weather";
        _.chain(Apperyio.mappings).filter(function(m) {
            return m.homeScreen === Apperyio.CurrentScreen;
        }).each(Apperyio.UIHandler.hideTemplateComponents);
    });

    Weather_onLoad();
};

$(document).off("pagecreate", "#Weather").on("pagecreate", "#Weather", function(event, ui) {
    Apperyio.processSelectMenu($(this));
    Weather_js();
});