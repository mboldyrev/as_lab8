.class public Lio/appery/project464000/MainActivity;
.super Lorg/apache/cordova/CordovaActivity;
.source "MainActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MainActivity"


# instance fields
.field private final isAutoupdateEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lorg/apache/cordova/CordovaActivity;-><init>()V

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/appery/project464000/MainActivity;->isAutoupdateEnabled:Z

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lio/appery/project464000/MainActivity;->moveTaskToBack(Z)Z

    .line 37
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lorg/apache/cordova/CordovaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    const/4 v2, 0x0

    .line 19
    .local v2, "startFileName":Ljava/lang/String;
    const-string v3, "MainActivity"

    const-string v4, "Init activity"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    iget-object v3, p0, Lio/appery/project464000/MainActivity;->preferences:Lorg/apache/cordova/CordovaPreferences;

    const-string v4, "SplashScreenDelay"

    const/16 v5, 0xbb8

    invoke-virtual {v3, v4, v5}, Lorg/apache/cordova/CordovaPreferences;->getInteger(Ljava/lang/String;I)I

    move-result v1

    .line 21
    .local v1, "splashscreenTime":I
    iget-object v3, p0, Lio/appery/project464000/MainActivity;->preferences:Lorg/apache/cordova/CordovaPreferences;

    const-string v4, "SplashScreenDelay"

    const v5, 0x186a0

    invoke-virtual {v3, v4, v5}, Lorg/apache/cordova/CordovaPreferences;->set(Ljava/lang/String;I)V

    .line 22
    invoke-virtual {p0}, Lio/appery/project464000/MainActivity;->init()V

    .line 24
    const-string v3, "MainActivity"

    const-string v4, "Start autoupdate"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    new-instance v0, Lio/appery/project464000/AutoUpdateTask;

    iget-object v3, p0, Lio/appery/project464000/MainActivity;->appView:Lorg/apache/cordova/CordovaWebView;

    invoke-direct {v0, p0, v3, v1}, Lio/appery/project464000/AutoUpdateTask;-><init>(Lorg/apache/cordova/CordovaActivity;Lorg/apache/cordova/CordovaWebView;I)V

    .line 26
    .local v0, "autoUpdateTask":Lio/appery/project464000/AutoUpdateTask;
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lio/appery/project464000/AutoUpdateTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 32
    return-void
.end method
