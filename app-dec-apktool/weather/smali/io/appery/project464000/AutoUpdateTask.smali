.class public Lio/appery/project464000/AutoUpdateTask;
.super Landroid/os/AsyncTask;
.source "AutoUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final ANGULAR_ASSETS_TO_COPY:[Ljava/lang/String;

.field private static final BUILD_TIMESTAMP_MILLISEC:Ljava/lang/Long;

.field private static final BUILD_TIME_PREF_NAME:Ljava/lang/String; = "BuildTime"

.field private static final CORDOVA_JQM_LIB_DIR:Ljava/lang/String; = "www/libs/jquerymobile/"

.field private static final FILES_DIR:Ljava/lang/String; = "/files"

.field private static final GET_BUNDLE_URL:Ljava/lang/String; = "https://upd.appery.io/update/47fefdf8-0aeb-46c2-a95e-d8f6c3864be8.zip"

.field private static final JQM_ASSETS_TO_COPY:[Ljava/lang/String;

.field private static final LIBS_DIR:Ljava/lang/String; = "www/libs/"

.field private static final TAG:Ljava/lang/String; = "AutoUpdateTask"

.field private static final WEB_RESOURCES_DIR:Ljava/lang/String; = "/www/"

.field public static final WORK_DIR:Ljava/lang/String; = "file:///android_asset/www/"


# instance fields
.field private activity:Lorg/apache/cordova/CordovaActivity;

.field private app_preferences:Landroid/content/SharedPreferences;

.field private hideSplashScreen:Z

.field private spinnerDialog:Landroid/app/ProgressDialog;

.field private splashscreenTime:I

.field private webView:Lorg/apache/cordova/CordovaWebView;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    const-wide v0, 0x15a5ad5b398L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lio/appery/project464000/AutoUpdateTask;->BUILD_TIMESTAMP_MILLISEC:Ljava/lang/Long;

    .line 37
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "cordova.js"

    aput-object v1, v0, v2

    const-string v1, "cordova_plugins.js"

    aput-object v1, v0, v3

    const-string v1, "get_target_platform.js"

    aput-object v1, v0, v4

    const-string v1, "plugins"

    aput-object v1, v0, v5

    sput-object v0, Lio/appery/project464000/AutoUpdateTask;->JQM_ASSETS_TO_COPY:[Ljava/lang/String;

    .line 44
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "cordova.js"

    aput-object v1, v0, v2

    const-string v1, "cordova_plugins.js"

    aput-object v1, v0, v3

    const-string v1, "plugins"

    aput-object v1, v0, v4

    sput-object v0, Lio/appery/project464000/AutoUpdateTask;->ANGULAR_ASSETS_TO_COPY:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/apache/cordova/CordovaActivity;Lorg/apache/cordova/CordovaWebView;I)V
    .locals 1
    .param p1, "activity"    # Lorg/apache/cordova/CordovaActivity;
    .param p2, "webView"    # Lorg/apache/cordova/CordovaWebView;
    .param p3, "splashscreenTime"    # I

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 68
    iput v0, p0, Lio/appery/project464000/AutoUpdateTask;->splashscreenTime:I

    .line 70
    iput-boolean v0, p0, Lio/appery/project464000/AutoUpdateTask;->hideSplashScreen:Z

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lio/appery/project464000/AutoUpdateTask;->spinnerDialog:Landroid/app/ProgressDialog;

    .line 75
    iput-object p1, p0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    .line 76
    iput-object p2, p0, Lio/appery/project464000/AutoUpdateTask;->webView:Lorg/apache/cordova/CordovaWebView;

    .line 77
    iput p3, p0, Lio/appery/project464000/AutoUpdateTask;->splashscreenTime:I

    .line 78
    return-void
.end method

.method static synthetic access$002(Lio/appery/project464000/AutoUpdateTask;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lio/appery/project464000/AutoUpdateTask;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 29
    iput-object p1, p0, Lio/appery/project464000/AutoUpdateTask;->spinnerDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$100(Lio/appery/project464000/AutoUpdateTask;)Z
    .locals 1
    .param p0, "x0"    # Lio/appery/project464000/AutoUpdateTask;

    .prologue
    .line 29
    iget-boolean v0, p0, Lio/appery/project464000/AutoUpdateTask;->hideSplashScreen:Z

    return v0
.end method

.method static synthetic access$102(Lio/appery/project464000/AutoUpdateTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lio/appery/project464000/AutoUpdateTask;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lio/appery/project464000/AutoUpdateTask;->hideSplashScreen:Z

    return p1
.end method

.method static synthetic access$200(Lio/appery/project464000/AutoUpdateTask;)V
    .locals 0
    .param p0, "x0"    # Lio/appery/project464000/AutoUpdateTask;

    .prologue
    .line 29
    invoke-direct {p0}, Lio/appery/project464000/AutoUpdateTask;->hideSplashScreenInAny()V

    return-void
.end method

.method private hideSplashScreenInAny()V
    .locals 7

    .prologue
    .line 195
    iget-object v2, p0, Lio/appery/project464000/AutoUpdateTask;->webView:Lorg/apache/cordova/CordovaWebView;

    invoke-interface {v2}, Lorg/apache/cordova/CordovaWebView;->getPluginManager()Lorg/apache/cordova/PluginManager;

    move-result-object v2

    const-string v3, "SplashScreen"

    invoke-virtual {v2, v3}, Lorg/apache/cordova/PluginManager;->getPlugin(Ljava/lang/String;)Lorg/apache/cordova/CordovaPlugin;

    move-result-object v1

    .line 196
    .local v1, "splashScreenPlugin":Lorg/apache/cordova/CordovaPlugin;
    if-eqz v1, :cond_0

    .line 198
    :try_start_0
    const-string v2, "hide"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    new-instance v4, Lorg/apache/cordova/CallbackContext;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lio/appery/project464000/AutoUpdateTask;->webView:Lorg/apache/cordova/CordovaWebView;

    invoke-direct {v4, v5, v6}, Lorg/apache/cordova/CallbackContext;-><init>(Ljava/lang/String;Lorg/apache/cordova/CordovaWebView;)V

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/cordova/CordovaPlugin;->execute(Ljava/lang/String;Lorg/json/JSONArray;Lorg/apache/cordova/CallbackContext;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "AutoUpdateTask"

    const-string v3, "Can\'t hide splashscreen"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private isConnectionActive()Z
    .locals 4

    .prologue
    .line 189
    iget-object v2, p0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lorg/apache/cordova/CordovaActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 190
    .local v0, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 191
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lio/appery/project464000/AutoUpdateTask;->doInBackground([Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/String;
    .locals 19
    .param p1, "params"    # [Ljava/lang/Long;

    .prologue
    .line 82
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lio/appery/project464000/AutoUpdateHelper;->DATA_DIR:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    invoke-virtual {v15}, Lorg/apache/cordova/CordovaActivity;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "baseDirectory":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    invoke-static {v14}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lio/appery/project464000/AutoUpdateTask;->app_preferences:Landroid/content/SharedPreferences;

    .line 85
    move-object/from16 v0, p0

    iget-object v14, v0, Lio/appery/project464000/AutoUpdateTask;->app_preferences:Landroid/content/SharedPreferences;

    const-string v15, "BuildTime"

    sget-object v16, Lio/appery/project464000/AutoUpdateTask;->BUILD_TIMESTAMP_MILLISEC:Ljava/lang/Long;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    invoke-interface/range {v14 .. v17}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 87
    .local v10, "modifiedDate":Ljava/lang/Long;
    invoke-direct/range {p0 .. p0}, Lio/appery/project464000/AutoUpdateTask;->isConnectionActive()Z

    move-result v14

    if-nez v14, :cond_1

    .line 88
    const-string v14, "AutoUpdateTask"

    const-string v15, "No connection"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    sget-object v14, Lio/appery/project464000/AutoUpdateTask;->BUILD_TIMESTAMP_MILLISEC:Ljava/lang/Long;

    invoke-virtual {v14, v10}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    const/4 v14, 0x0

    .line 142
    .end local v10    # "modifiedDate":Ljava/lang/Long;
    :goto_0
    return-object v14

    .line 89
    .restart local v10    # "modifiedDate":Ljava/lang/Long;
    :cond_0
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/www/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto :goto_0

    .line 92
    :cond_1
    new-instance v12, Ljava/net/URL;

    const-string v14, "https://upd.appery.io/update/47fefdf8-0aeb-46c2-a95e-d8f6c3864be8.zip"

    invoke-direct {v12, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 94
    .local v12, "url":Ljava/net/URL;
    const-string v14, "AutoUpdateTask"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Get autoupdate from: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v12}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-virtual {v12}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    .line 97
    .local v4, "conn":Ljava/net/HttpURLConnection;
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v4, v14, v15}, Ljava/net/HttpURLConnection;->setIfModifiedSince(J)V

    .line 98
    const v14, 0x2bf20

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 99
    const/16 v14, 0x5dc

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 100
    const-string v14, "GET"

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 101
    const/4 v14, 0x1

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 102
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 104
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v11

    .line 105
    .local v11, "responseCode":I
    const-string v14, "AutoUpdateTask"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "The response code is: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/4 v13, 0x0

    .line 109
    .local v13, "workingBundleDir":Ljava/lang/String;
    const/16 v14, 0xc8

    if-ne v14, v11, :cond_4

    .line 110
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    .line 111
    .local v9, "is":Ljava/io/InputStream;
    if-eqz v9, :cond_2

    .line 112
    const-string v14, "AutoUpdateTask"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Load autoupdate bundle from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v12}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/files"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v9, v14}, Lio/appery/project464000/AutoUpdateHelper;->downloadBundle(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 114
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 116
    const-string v14, "AutoUpdateTask"

    const-string v15, "Extract autoupdate bundle"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/files"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/www/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lio/appery/project464000/AutoUpdateHelper;->unzip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 120
    :cond_2
    sget-object v2, Lio/appery/project464000/AutoUpdateTask;->JQM_ASSETS_TO_COPY:[Ljava/lang/String;

    .line 121
    .local v2, "assetsToCopy":[Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    const-string v14, "www/libs/jquerymobile/"

    invoke-direct {v5, v3, v14}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .local v5, "dir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_3

    .line 123
    sget-object v2, Lio/appery/project464000/AutoUpdateTask;->ANGULAR_ASSETS_TO_COPY:[Ljava/lang/String;

    .line 126
    :cond_3
    array-length v15, v2

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v15, :cond_6

    aget-object v8, v2, v14

    .line 127
    .local v8, "fName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "www/libs/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lio/appery/project464000/AutoUpdateHelper;->copyFileOrDir(Landroid/content/Context;Ljava/lang/String;)V

    .line 126
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 130
    .end local v2    # "assetsToCopy":[Ljava/lang/String;
    .end local v5    # "dir":Ljava/io/File;
    .end local v8    # "fName":Ljava/lang/String;
    .end local v9    # "is":Ljava/io/InputStream;
    :cond_4
    sget-object v14, Lio/appery/project464000/AutoUpdateTask;->BUILD_TIMESTAMP_MILLISEC:Ljava/lang/Long;

    invoke-virtual {v14, v10}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_5
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/www/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0

    .line 133
    .restart local v2    # "assetsToCopy":[Ljava/lang/String;
    .restart local v5    # "dir":Ljava/io/File;
    .restart local v9    # "is":Ljava/io/InputStream;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lio/appery/project464000/AutoUpdateTask;->app_preferences:Landroid/content/SharedPreferences;

    const-string v15, "BuildTime"

    const-wide/16 v16, 0x0

    invoke-interface/range {v14 .. v17}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    .line 134
    move-object/from16 v0, p0

    iget-object v14, v0, Lio/appery/project464000/AutoUpdateTask;->app_preferences:Landroid/content/SharedPreferences;

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 135
    .local v7, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "BuildTime"

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getLastModified()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-interface {v7, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 136
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v14, v13

    .line 138
    goto/16 :goto_0

    .line 140
    .end local v2    # "assetsToCopy":[Ljava/lang/String;
    .end local v4    # "conn":Ljava/net/HttpURLConnection;
    .end local v5    # "dir":Ljava/io/File;
    .end local v7    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v9    # "is":Ljava/io/InputStream;
    .end local v10    # "modifiedDate":Ljava/lang/Long;
    .end local v11    # "responseCode":I
    .end local v12    # "url":Ljava/net/URL;
    .end local v13    # "workingBundleDir":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 141
    .local v6, "e":Ljava/io/IOException;
    const-string v14, "AutoUpdateTask"

    const-string v15, "Autoupdate failed"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 142
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lio/appery/project464000/AutoUpdateTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v1, p0, Lio/appery/project464000/AutoUpdateTask;->spinnerDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lio/appery/project464000/AutoUpdateTask;->spinnerDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lio/appery/project464000/AutoUpdateTask;->spinnerDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 174
    const/4 v1, 0x0

    iput-object v1, p0, Lio/appery/project464000/AutoUpdateTask;->spinnerDialog:Landroid/app/ProgressDialog;

    .line 177
    :cond_0
    iget-boolean v1, p0, Lio/appery/project464000/AutoUpdateTask;->hideSplashScreen:Z

    if-eqz v1, :cond_1

    .line 178
    invoke-direct {p0}, Lio/appery/project464000/AutoUpdateTask;->hideSplashScreenInAny()V

    .line 183
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "index.html"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "filePath":Ljava/lang/String;
    const-string v1, "AutoUpdateTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Open URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v1, p0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    invoke-virtual {v1, v0}, Lorg/apache/cordova/CordovaActivity;->loadUrl(Ljava/lang/String;)V

    .line 186
    return-void

    .line 180
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lio/appery/project464000/AutoUpdateTask;->hideSplashScreen:Z

    goto :goto_0

    .line 183
    :cond_2
    const-string v1, "file:///android_asset/www/"

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 149
    iget-object v0, p0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    iget-object v1, p0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    const v2, 0x7f060090

    invoke-virtual {v1, v2}, Lorg/apache/cordova/CordovaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lio/appery/project464000/AutoUpdateTask;->activity:Lorg/apache/cordova/CordovaActivity;

    const v4, 0x7f06008f

    invoke-virtual {v2, v4}, Lorg/apache/cordova/CordovaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lio/appery/project464000/AutoUpdateTask$1;

    invoke-direct {v5, p0}, Lio/appery/project464000/AutoUpdateTask$1;-><init>(Lio/appery/project464000/AutoUpdateTask;)V

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lio/appery/project464000/AutoUpdateTask;->spinnerDialog:Landroid/app/ProgressDialog;

    .line 156
    iget v0, p0, Lio/appery/project464000/AutoUpdateTask;->splashscreenTime:I

    if-lez v0, :cond_0

    .line 157
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    .line 158
    .local v6, "handler":Landroid/os/Handler;
    new-instance v0, Lio/appery/project464000/AutoUpdateTask$2;

    invoke-direct {v0, p0}, Lio/appery/project464000/AutoUpdateTask$2;-><init>(Lio/appery/project464000/AutoUpdateTask;)V

    iget v1, p0, Lio/appery/project464000/AutoUpdateTask;->splashscreenTime:I

    int-to-long v2, v1

    invoke-virtual {v6, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 168
    .end local v6    # "handler":Landroid/os/Handler;
    :cond_0
    return-void
.end method
