.class public final Lbarcodescanner/xservices/nl/barcodescanner/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbarcodescanner/xservices/nl/barcodescanner/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f06008e

.field public static final app_picker_name:I = 0x7f060011

.field public static final bookmark_picker_name:I = 0x7f060012

.field public static final button_add_calendar:I = 0x7f060013

.field public static final button_add_contact:I = 0x7f060014

.field public static final button_book_search:I = 0x7f060015

.field public static final button_cancel:I = 0x7f060016

.field public static final button_custom_product_search:I = 0x7f060017

.field public static final button_dial:I = 0x7f060018

.field public static final button_email:I = 0x7f060019

.field public static final button_get_directions:I = 0x7f06001a

.field public static final button_mms:I = 0x7f06001b

.field public static final button_ok:I = 0x7f06001c

.field public static final button_open_browser:I = 0x7f06001d

.field public static final button_product_search:I = 0x7f06001e

.field public static final button_search_book_contents:I = 0x7f06001f

.field public static final button_share_app:I = 0x7f060020

.field public static final button_share_bookmark:I = 0x7f060021

.field public static final button_share_by_email:I = 0x7f060022

.field public static final button_share_by_sms:I = 0x7f060023

.field public static final button_share_clipboard:I = 0x7f060024

.field public static final button_share_contact:I = 0x7f060025

.field public static final button_show_map:I = 0x7f060026

.field public static final button_sms:I = 0x7f060027

.field public static final button_web_search:I = 0x7f060028

.field public static final button_wifi:I = 0x7f060029

.field public static final contents_contact:I = 0x7f06002a

.field public static final contents_email:I = 0x7f06002b

.field public static final contents_location:I = 0x7f06002c

.field public static final contents_phone:I = 0x7f06002d

.field public static final contents_sms:I = 0x7f06002e

.field public static final contents_text:I = 0x7f06002f

.field public static final history_clear_one_history_text:I = 0x7f060030

.field public static final history_clear_text:I = 0x7f060031

.field public static final history_email_title:I = 0x7f060032

.field public static final history_empty:I = 0x7f060033

.field public static final history_empty_detail:I = 0x7f060034

.field public static final history_send:I = 0x7f060035

.field public static final history_title:I = 0x7f060036

.field public static final menu_encode_mecard:I = 0x7f060037

.field public static final menu_encode_vcard:I = 0x7f060038

.field public static final menu_help:I = 0x7f060039

.field public static final menu_history:I = 0x7f06003a

.field public static final menu_settings:I = 0x7f06003b

.field public static final menu_share:I = 0x7f06003c

.field public static final msg_bulk_mode_scanned:I = 0x7f06003d

.field public static final msg_camera_framework_bug:I = 0x7f06003e

.field public static final msg_default_format:I = 0x7f06003f

.field public static final msg_default_meta:I = 0x7f060040

.field public static final msg_default_mms_subject:I = 0x7f060041

.field public static final msg_default_status:I = 0x7f060042

.field public static final msg_default_time:I = 0x7f060043

.field public static final msg_default_type:I = 0x7f060044

.field public static final msg_encode_contents_failed:I = 0x7f060045

.field public static final msg_error:I = 0x7f060046

.field public static final msg_google_books:I = 0x7f060047

.field public static final msg_google_product:I = 0x7f060048

.field public static final msg_intent_failed:I = 0x7f060049

.field public static final msg_invalid_value:I = 0x7f06004a

.field public static final msg_redirect:I = 0x7f06004b

.field public static final msg_sbc_book_not_searchable:I = 0x7f06004c

.field public static final msg_sbc_failed:I = 0x7f06004d

.field public static final msg_sbc_no_page_returned:I = 0x7f06004e

.field public static final msg_sbc_page:I = 0x7f06004f

.field public static final msg_sbc_results:I = 0x7f060050

.field public static final msg_sbc_searching_book:I = 0x7f060051

.field public static final msg_sbc_snippet_unavailable:I = 0x7f060052

.field public static final msg_share_explanation:I = 0x7f060053

.field public static final msg_share_text:I = 0x7f060054

.field public static final msg_sure:I = 0x7f060055

.field public static final msg_unmount_usb:I = 0x7f060056

.field public static final preferences_actions_title:I = 0x7f060057

.field public static final preferences_auto_focus_title:I = 0x7f060058

.field public static final preferences_auto_open_web_title:I = 0x7f060059

.field public static final preferences_bulk_mode_summary:I = 0x7f06005a

.field public static final preferences_bulk_mode_title:I = 0x7f06005b

.field public static final preferences_copy_to_clipboard_title:I = 0x7f06005c

.field public static final preferences_custom_product_search_summary:I = 0x7f06005d

.field public static final preferences_custom_product_search_title:I = 0x7f06005e

.field public static final preferences_decode_1D_industrial_title:I = 0x7f06005f

.field public static final preferences_decode_1D_product_title:I = 0x7f060060

.field public static final preferences_decode_Aztec_title:I = 0x7f060061

.field public static final preferences_decode_Data_Matrix_title:I = 0x7f060062

.field public static final preferences_decode_PDF417_title:I = 0x7f060063

.field public static final preferences_decode_QR_title:I = 0x7f060064

.field public static final preferences_device_bug_workarounds_title:I = 0x7f060065

.field public static final preferences_disable_barcode_scene_mode_title:I = 0x7f060066

.field public static final preferences_disable_continuous_focus_summary:I = 0x7f060067

.field public static final preferences_disable_continuous_focus_title:I = 0x7f060068

.field public static final preferences_disable_exposure_title:I = 0x7f060069

.field public static final preferences_disable_metering_title:I = 0x7f06006a

.field public static final preferences_front_light_auto:I = 0x7f06006b

.field public static final preferences_front_light_off:I = 0x7f06006c

.field public static final preferences_front_light_on:I = 0x7f06006d

.field public static final preferences_front_light_summary:I = 0x7f06006e

.field public static final preferences_front_light_title:I = 0x7f06006f

.field public static final preferences_general_title:I = 0x7f060070

.field public static final preferences_history_summary:I = 0x7f060071

.field public static final preferences_history_title:I = 0x7f060072

.field public static final preferences_invert_scan_summary:I = 0x7f060073

.field public static final preferences_invert_scan_title:I = 0x7f060074

.field public static final preferences_name:I = 0x7f060075

.field public static final preferences_orientation_title:I = 0x7f060076

.field public static final preferences_play_beep_title:I = 0x7f060077

.field public static final preferences_remember_duplicates_summary:I = 0x7f060078

.field public static final preferences_remember_duplicates_title:I = 0x7f060079

.field public static final preferences_result_title:I = 0x7f06007a

.field public static final preferences_scanning_title:I = 0x7f06007b

.field public static final preferences_search_country:I = 0x7f06007c

.field public static final preferences_supplemental_summary:I = 0x7f06007d

.field public static final preferences_supplemental_title:I = 0x7f06007e

.field public static final preferences_vibrate_title:I = 0x7f06007f

.field public static final result_address_book:I = 0x7f060080

.field public static final result_calendar:I = 0x7f060081

.field public static final result_email_address:I = 0x7f060082

.field public static final result_geo:I = 0x7f060083

.field public static final result_isbn:I = 0x7f060084

.field public static final result_product:I = 0x7f060085

.field public static final result_sms:I = 0x7f060086

.field public static final result_tel:I = 0x7f060087

.field public static final result_text:I = 0x7f060088

.field public static final result_uri:I = 0x7f060089

.field public static final result_wifi:I = 0x7f06008a

.field public static final sbc_name:I = 0x7f06008b

.field public static final wifi_changing_network:I = 0x7f06008c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
