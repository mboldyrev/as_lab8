.class public final Lbarcodescanner/xservices/nl/barcodescanner/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbarcodescanner/xservices/nl/barcodescanner/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final app_picker_list_item_icon:I = 0x7f0a0010

.field public static final app_picker_list_item_label:I = 0x7f0a0011

.field public static final barcode_image_view:I = 0x7f0a0017

.field public static final bookmark_title:I = 0x7f0a0012

.field public static final bookmark_url:I = 0x7f0a0013

.field public static final contents_supplement_text_view:I = 0x7f0a001e

.field public static final contents_text_view:I = 0x7f0a001d

.field public static final decode:I = 0x7f0a0000

.field public static final decode_failed:I = 0x7f0a0001

.field public static final decode_succeeded:I = 0x7f0a0002

.field public static final flip_button:I = 0x7f0a0021

.field public static final format_text_view:I = 0x7f0a0018

.field public static final help_contents:I = 0x7f0a0023

.field public static final history_detail:I = 0x7f0a0025

.field public static final history_title:I = 0x7f0a0024

.field public static final image_view:I = 0x7f0a0022

.field public static final launch_product_query:I = 0x7f0a0003

.field public static final menu_encode:I = 0x7f0a0034

.field public static final menu_help:I = 0x7f0a0033

.field public static final menu_history:I = 0x7f0a0031

.field public static final menu_history_clear_text:I = 0x7f0a0036

.field public static final menu_history_send:I = 0x7f0a0035

.field public static final menu_settings:I = 0x7f0a0032

.field public static final menu_share:I = 0x7f0a0030

.field public static final meta_text_view:I = 0x7f0a001c

.field public static final meta_text_view_label:I = 0x7f0a001b

.field public static final none:I = 0x7f0a0009

.field public static final page_number_view:I = 0x7f0a0029

.field public static final preview_view:I = 0x7f0a0014

.field public static final query_button:I = 0x7f0a0027

.field public static final query_text_view:I = 0x7f0a0026

.field public static final quit:I = 0x7f0a0004

.field public static final restart_preview:I = 0x7f0a0005

.field public static final result_button_view:I = 0x7f0a001f

.field public static final result_list_view:I = 0x7f0a0028

.field public static final result_view:I = 0x7f0a0016

.field public static final return_scan_result:I = 0x7f0a0006

.field public static final share_app_button:I = 0x7f0a002b

.field public static final share_bookmark_button:I = 0x7f0a002c

.field public static final share_clipboard_button:I = 0x7f0a002e

.field public static final share_contact_button:I = 0x7f0a002d

.field public static final share_text_view:I = 0x7f0a002f

.field public static final snippet_view:I = 0x7f0a002a

.field public static final status_view:I = 0x7f0a0020

.field public static final time_text_view:I = 0x7f0a001a

.field public static final type_text_view:I = 0x7f0a0019

.field public static final viewfinder_view:I = 0x7f0a0015


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
