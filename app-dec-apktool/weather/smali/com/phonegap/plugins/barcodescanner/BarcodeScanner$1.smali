.class Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/phonegap/plugins/barcodescanner/BarcodeScanner;->scan(Lorg/json/JSONArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/phonegap/plugins/barcodescanner/BarcodeScanner;

.field final synthetic val$args:Lorg/json/JSONArray;

.field final synthetic val$that:Lorg/apache/cordova/CordovaPlugin;


# direct methods
.method constructor <init>(Lcom/phonegap/plugins/barcodescanner/BarcodeScanner;Lorg/json/JSONArray;Lorg/apache/cordova/CordovaPlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/phonegap/plugins/barcodescanner/BarcodeScanner;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->this$0:Lcom/phonegap/plugins/barcodescanner/BarcodeScanner;

    iput-object p2, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$args:Lorg/json/JSONArray;

    iput-object p3, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$that:Lorg/apache/cordova/CordovaPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 136
    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.google.zxing.client.android.SCAN"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 137
    .local v2, "intentScan":Landroid/content/Intent;
    const-string v8, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    iget-object v8, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$args:Lorg/json/JSONArray;

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-lez v8, :cond_7

    .line 147
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v8, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$args:Lorg/json/JSONArray;

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v1, v8, :cond_7

    .line 150
    :try_start_0
    iget-object v8, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$args:Lorg/json/JSONArray;

    invoke-virtual {v8, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 156
    .local v6, "obj":Lorg/json/JSONObject;
    invoke-virtual {v6}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v5

    .line 157
    .local v5, "names":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v3, v8, :cond_3

    .line 159
    :try_start_1
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 160
    .local v4, "key":Ljava/lang/String;
    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 162
    .local v7, "value":Ljava/lang/Object;
    instance-of v8, v7, Ljava/lang/Integer;

    if-eqz v8, :cond_2

    .line 163
    check-cast v7, Ljava/lang/Integer;

    .end local v7    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 157
    .end local v4    # "key":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 151
    .end local v3    # "j":I
    .end local v5    # "names":Lorg/json/JSONArray;
    .end local v6    # "obj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lorg/json/JSONException;
    const-string v8, "CordovaLog"

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    .restart local v3    # "j":I
    .restart local v4    # "key":Ljava/lang/String;
    .restart local v5    # "names":Lorg/json/JSONArray;
    .restart local v6    # "obj":Lorg/json/JSONObject;
    .restart local v7    # "value":Ljava/lang/Object;
    :cond_2
    :try_start_2
    instance-of v8, v7, Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 165
    check-cast v7, Ljava/lang/String;

    .end local v7    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 168
    .end local v4    # "key":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 169
    .restart local v0    # "e":Lorg/json/JSONException;
    const-string v8, "CordovaLog"

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 173
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_3
    const-string v10, "SCAN_CAMERA_ID"

    const-string v8, "preferFrontCamera"

    invoke-virtual {v6, v8, v9}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v8, 0x1

    :goto_4
    invoke-virtual {v2, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const-string v8, "SHOW_FLIP_CAMERA_BUTTON"

    const-string v10, "showFlipCameraButton"

    invoke-virtual {v6, v10, v9}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v10

    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 175
    const-string v8, "formats"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 176
    const-string v8, "SCAN_FORMATS"

    const-string v10, "formats"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    :cond_4
    const-string v8, "prompt"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 179
    const-string v8, "PROMPT_MESSAGE"

    const-string v10, "prompt"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    :cond_5
    const-string v8, "orientation"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 182
    const-string v8, "ORIENTATION_LOCK"

    const-string v10, "orientation"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    :cond_6
    move v8, v9

    .line 173
    goto :goto_4

    .line 189
    .end local v1    # "i":I
    .end local v3    # "j":I
    .end local v5    # "names":Lorg/json/JSONArray;
    .end local v6    # "obj":Lorg/json/JSONObject;
    :cond_7
    iget-object v8, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$that:Lorg/apache/cordova/CordovaPlugin;

    iget-object v8, v8, Lorg/apache/cordova/CordovaPlugin;->cordova:Lorg/apache/cordova/CordovaInterface;

    invoke-interface {v8}, Lorg/apache/cordova/CordovaInterface;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    iget-object v8, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$that:Lorg/apache/cordova/CordovaPlugin;

    iget-object v8, v8, Lorg/apache/cordova/CordovaPlugin;->cordova:Lorg/apache/cordova/CordovaInterface;

    iget-object v9, p0, Lcom/phonegap/plugins/barcodescanner/BarcodeScanner$1;->val$that:Lorg/apache/cordova/CordovaPlugin;

    const v10, 0xba7c0de

    invoke-interface {v8, v9, v2, v10}, Lorg/apache/cordova/CordovaInterface;->startActivityForResult(Lorg/apache/cordova/CordovaPlugin;Landroid/content/Intent;I)V

    .line 192
    return-void
.end method
